import tkinter.simpledialog as tksd
import tkinter.messagebox as tkmb

debajo=0
normal=0
sobresaliente=0
n=0
band=True
while band:
    c=tksd.askinteger(title='Entrada de datos de trabajador', prompt='Proporciona un entero[1..5]', minvalue=1, maxvalue=5)
    n+=1
    if c<3:
        debajo+=1
    elif c<5:
        normal+=1
    else:
        sobresaliente+=1
    band=tkmb.askyesno(title='Entrada de datos', message='Existen más empleados?')

salida=f'% por debajo son={debajo*100/n}%'
salida+=f'\n% normal son={normal*100/n}%'
salida+=f'\n% sobresaliente son={sobresaliente*100/n}%'
tkmb.showinfo(title="Resultados", message=salida)


