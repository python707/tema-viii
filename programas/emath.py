# Autor
# Descripción: Programa que muestra el uso de funciones de la biblioteca math

import math

r=float(input('Proporciona radio de circunferencia:'))

area=math.pi*math.pow(r,2)

print(f'El área de una circunferencia con radio={r} es={area}')