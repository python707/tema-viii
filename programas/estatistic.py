# Autor
# Descripción: Programa que muestra el uso de funciones de la biblioteca statistics

import statistics

lista=[]
resp='s'
while resp=='s':
    item=float(input('Proporciona un número:'))
    lista.append(item)
    resp=input('Existen más datos:[s/n]:')

print(f'El promedio de los datos es:{statistics.median(lista)}')
moda=statistics.mode(lista)
print(f'La moda de la lista es {moda}')
print(f'La desviación estandar es:{statistics.stdev(lista)}')
