# Autor
# Descripción: Programa que muestra el uso de funciones de la biblioteca random

import random
tiroBase= random.randint(2,12)
band=True
gano=False
tiros=0
while band:
    tiroNuevo=random.randint(2,12)
    tiros+=1
    if tiroNuevo==tiroBase:
        band=False
        gano=True
    elif  any([tiroNuevo==x for x in [7,8,9,10,11] ]):
      band=False
if gano:
    print(f'Gano en {tiros} tiradas')
else:
    print(f'Ha perdido en {tiros} tiradas')
