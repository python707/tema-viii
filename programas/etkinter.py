from tkinter import * 
from tkinter import ttk
from random import randint




class MyApp(ttk.Frame):
    def __init__(self,root):
        super().__init__(root,width=300,height=200)
        self.root=root
        self.root.title('Mi App')
        self.genMenu(self.root)
        self.imagenes=['./img/delfin.png',
                       './img/oso.png',
                       './img/ardilla.png',
                       './img/ballena.png',
                       './img/aguila.png',
                       './img/guepardo.png']
        
        self.bar=[-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]
        self.b=[ttk.Button(self,text='  1  ',command=self.juega),
                ttk.Button(self,text='  2  ',command=lambda:self.juega(1)),
                ttk.Button(self,text='  3  ',command=lambda:self.juega(2)),
                ttk.Button(self,text='  4  ',command=lambda:self.juega(3)),
                ttk.Button(self,text='  5  ',command=lambda:self.juega(4)),
                ttk.Button(self,text='  6  ',command=lambda:self.juega(5)),
                ttk.Button(self,text='  7  ',command=lambda:self.juega(6)),
                ttk.Button(self,text='  8  ',command=lambda:self.juega(7)),
                ttk.Button(self,text='  9  ',command=lambda:self.juega(8)),
                ttk.Button(self,text='  10  ',command=lambda:self.juega(9)),
                ttk.Button(self,text='  11  ',command=lambda:self.juega(10)),
                ttk.Button(self,text='  12  ',command=lambda:self.juega(11))
                ]
        self.barajea()
        self.abierta=False
        self.indice=-1
        self.root.resizable(0,0)
        self.pack()
        
    def genMenu(self,root):
            barraMenu= Menu(root)
            root.config(menu=barraMenu,width=300, height=300)
            menuInicio=Menu(barraMenu,tearoff=0)
            barraMenu.add_cascade(label='Inicio',menu=menuInicio)

            menuInicio.add_command(label='Reiniciar juego',command=self.reiniciar)
            menuInicio.add_command(label='Salir',command=root.destroy)

    def coloca(self):
        i=0
        for y in range(6):
            for x in range(2):
              while True:
                t=randint(0,11)
                if self.bar[t]==-1:
                    self.bar[t]=i
                    break
            i+=1


    def barajea(self):
        self.b[0].grid(row=0,column=0, padx=10, pady=10)
        self.b[1].grid(row=0,column=1, padx=10, pady=10)
        self.b[2].grid(row=0,column=2, padx=10, pady=10)        
        self.b[3].grid(row=0,column=3, padx=10, pady=10) 
        self.b[4].grid(row=1,column=0, padx=10, pady=10)  
        self.b[5].grid(row=1,column=1, padx=10, pady=10) 
        self.b[6].grid(row=1,column=2, padx=10, pady=10)
        self.b[7].grid(row=1,column=3, padx=10, pady=10)
        self.b[8].grid(row=2,column=0, padx=10, pady=10) 
        self.b[9].grid(row=2,column=1, padx=10, pady=10) 
        self.b[10].grid(row=2,column=2, padx=10, pady=10)
        self.b[11].grid(row=2,column=3, padx=10, pady=10)
        self.coloca()
 
    def juega(self,i=0):
        if self.abierta:
              if self.bar[self.indice]==self.bar[i]:
                   imagen=PhotoImage(file=self.imagenes[self.bar[i]])
                   self.b[i].config(image=imagen)
                   self.b[i].image=imagen
              else:
                  self.b[self.indice].image=None
              self.indice=-1
              self.abierta=False 
        else:
            self.indice=i
            self.abierta=True
            imagen=PhotoImage(file=self.imagenes[self.bar[i]])
            self.b[i].config(image=imagen)
            self.b[i].image=imagen

    def reiniciar(self):
        for i in range(12):
           self.b[i].image=None
        self.bar=[-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]
        self.coloca()     


def main():
   root = Tk()
   f= MyApp(root)
   root.mainloop()

if __name__=='__main__':
    main()
 